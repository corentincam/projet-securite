# Récapitulatif

Voici un récapitulatif des diverses informations découvertes dans la clé USB de notre suspect Pascal Sophia CARDIN.

## Informations générales

- Mot de passe `Perso.xlsx` : 060819677940
- Mot de passe `Photos/Photos.7z` : 79402222

## Coordonnées bancaires

Code de carte bancaire : 7940

IBAN : FR76 30003 00345 0052 2637 700 75

Société Générale, agence au 35 Avenue Gambetta à Blois.

*Le numéro de compte 00522637700 ne semble pas respecter la somme de contrôle usuelle.*

Coordonnées des autres comptes :

- En **Espagne** (chez Caixa) : 2345T67A22ZRET56
- En **Norvège** (HSBC) : FSK67YESHYYESSSFDYTU
- Au **Danemark** (HSBC) : FSK67YESHYYESSYYESHA
- En **Suède** (HSBC) : FSK67YESHYYESSYEDDRS
- En **Thaïlande** (KasikornBank) : ERT-ERTS765

## Historique des déplacements

Tous les déplacements trouvés nous donnent un bon aperçu des pratiques douteuses du suspect.

Ils confirment aussi les localisations trouvées dans le fichier Excel (Espagne, Norvège, Danemark, Thailande)
