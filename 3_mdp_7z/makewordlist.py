from itertools import permutations

words = ['2222', '7940', 'SG', '30003345522637700', '75']

with open('wordlist', 'w') as f:
    for i in range(1, 7):
        for w in permutations(words, i):
            f.write(''.join(w) + '\n')
