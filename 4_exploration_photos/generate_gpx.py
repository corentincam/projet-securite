import os
import sys
import gpxpy.gpx
from GPSPhoto import gpsphoto

FOLDER = sys.argv[1]

gpx = gpxpy.gpx.GPX()

gpx_track = gpxpy.gpx.GPXTrack()
gpx.tracks.append(gpx_track)

gpx_segment = gpxpy.gpx.GPXTrackSegment()
gpx_track.segments.append(gpx_segment)

datas = []

for f in os.listdir(FOLDER):
    data = gpsphoto.getGPSData(os.path.join(FOLDER, f))
    data['time'] = os.path.getmtime(os.path.join(FOLDER, f))
    datas.append(data)

datas.sort(key=lambda x: x['time'])

for data in datas:
    gpx_segment.points.append(
        gpxpy.gpx.GPXTrackPoint(
            data['Latitude'], data['Longitude'], elevation=data['Altitude']))

with open('trace.gpx', 'w') as f:
    f.write(gpx.to_xml())
