# Projet Sécurité

Dans le cadre de l'UE Sécurité et Informatique Légale, nous avons participé à un projet consistant à trouver des preuves d'agissement parmi des fichiers contenus sur une clé USB (plus de détails dans le fichier `Sujet.pdf`).

Ce dépôt est partagé en plusieurs dossiers représentant les différentes phases de notre recherche.

- `1_mdp_excel` : Première appréhension du projet, tentative d'ouverture du fichier `Perso.xlsx`
- `2_exploration_excel` : Exploration du fichier Excel, découvertes intéressantes...
- `3_mdp_7z` : Recherche du mot de passe de l'archive `Photos.7z`
- `4_exploration_photos` : Exploration du dossier de photos et exploitation des données trouvées.
- `5_recap` : Récapitulatif des informations trouvées.

Ces différentes parties peuvent être lues dans n'importe quel ordre.

## Groupe

Louis BERLIC ([@Loison](https://gitlab.com/Loison))
Corentin CAM ([@valtrok](https://gitlab.com/valtrok))

ENSEEIHT - 3e année Sciences du Numérique - Parcours Logiciel
