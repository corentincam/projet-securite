# Exploration du fichier Excel

Après ouverture du fichier Excel avec le mot de passe précédemment trouvé, nous pouvons noter dès le début qu'il y a trois différentes feuilles, dont deux protégées.

## Feuille 2

### Codes

Cette feuille est la première à apparaître lors de l'ouverture du fichier, on peut vite voir que toutes les cellules "nécessaires" sont remplies, mais certaines en blanc.
La cellule B3 contient ce qui devrait être le code de carte bancaire, mais il est différent de celui trouvé plus tôt (7940). Cependant on remarque que la cellule juste à droite contient la chaîne `+1`. En effet 6839 devient 7940 en ajoutant 1 à chaque chiffre (modulo 10).

Grâce à cela, nous pouvons déduire que le code PIN de la case d'en dessous devient donc 2222 (en enlevant 1 comme `-1` le suggère dans la case voisine). Ais à quoi peut-il bien servir ?

### Coordonnées bancaires

Les coordonnées bancaires pourraient aussi être utiles. 'SG' pourrait correspondre à Société Générale par exemple.

Les chiffres présents en dessous confirment ceci : une fois décortiqués, on peut reconnaître une partie d'un IBAN : 

| Code banque | Code guichet | Numéro de compte | Clé |
|-------------|--------------|------------------|-----|
|       30003 |        34552 | (0000)2637700    |  75 |

Le code banque correspond bien à celui de la Société Générale.

Le code guichet cependant ne correspond à aucune agence...
En ajoutant des zéros au début du code guichet et en déplaçant le reste on arrive à : 

| Code banque | Code guichet | Numéro de compte | Clé |
|-------------|--------------|------------------|-----|
|       30003 | (00)345      | (00)522637700    |  75 |

Le code 00345 correspond à une agence au 35 Avenue Gambetta à Blois.

En ajoutant FR76 devant on peut le tester facilement sur un site spécialisé et s'apercevoir que le numéro de compte n'est pas correct, tout comme la somme de contrôle. Erreur de la part du suspect ou manque d'informations ? Mystère...

## Feuille 3

La feuille 3 étant totalement protégée et inexploitable en l'état, nous sauvons une copie non protégée du fichier et la faisons ouvrir par Google Sheets (ce dernier ne gérant pas les protections de feuilles, il les fait sauter par défaut).

Cette feuille, après révélation du texte (Ctrl-A, Tout en noir police 14), révèle de très intéressantes informations.

Il est en effet fait allusion à des paiements de différentes sources dans des pays différents, allant de 50 000 à 200 000€. Le type de paiement est aussi mentionné, toujours stupéfiants ou prostitutions. On dirait qu'on a affaire à un gros poisson !

Voici l'intégralité des données trouvées sur la feuille 3 :

- État des paiements

| Pays      | Banque       | Compte               | Montant      |
|-----------|--------------|----------------------|--------------|
| Norvège   | HSBC         | FSK67YESHYYESSSFDYTU | 50 000,00 €  |
| Danemark  | HSBC         | FSK67YESHYYESSYYESHA | 60 000,00 €  |
| Suède     | HSBC         | FSK67YESHYYESSYEDDRS | 80 000,00 €  |
| Thailande | KasikornBank | ERT-ERTS765          | 200 000,00 € |

- Sources

| Pays      |                           |
|-----------|---------------------------|
| Espagne   | Stupéfiants               |
| Norvège   | Stupéfiants               |
| Danemark  | Stupéfiants/Prostitutions |
| Suède     | Stupéfiants/Prostitutions |
| Thailande | Prostitutions             |

## Feuille 1

La feuille 1 ne comprend aucune information.

## Vérification de l'absence de données cachées supplémentaires

Pour vérifier que nous avions exploré toutes les données contenues dans le fichier, il a suffit d'écrire une fonction dans n'importe quelle case pour chaque feuille, par exemple `=COUNTA(Feuil1!A:ZZ)` puis compter les cases réellement explorées et comparer.
